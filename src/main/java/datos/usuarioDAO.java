package datos;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import javax.persistence.Query;

import modelo.Usuario;


@Stateless
public class usuarioDAO {
	
	@Inject
	private EntityManager em;
	
	public void insert(Usuario usuario)
	{
		em.persist(usuario);
		
	}
	
	public void update(Usuario usuario)
	{
		em.merge(usuario);
		
	}
	
	public void remove(int codigo)
	{
		Usuario usuario=this.read(codigo);
		em.remove(usuario);
		
	}
	
	public Usuario read(int codigo)
	{
		Usuario c=em.find(Usuario.class, codigo);
		return c;
	}
	
	
	//consulta a una BD, se hace refrencia a las entidades del paquete modelo, no a la BD
	public List<Usuario> getusuarios()
	{
		String jpql = "SELECT u FROM Usuario u";
		Query q = em.createQuery(jpql, Usuario.class);
		List<Usuario> usuarios = q.getResultList();
		return usuarios;
		
	}
	
	
	public List<Usuario> getusuariosNombre(String filtro)
	{
		String jpql = "SELECT u FROM Usuario u WHERE nombre LIKE ?1 ";
		Query q = em.createQuery(jpql, Usuario.class);
		q.setParameter(1, "%"+filtro+"%");
		List<Usuario> usuarios = q.getResultList();
		return usuarios;
		
	}
	
	public List<Usuario> getusuariosCorreo(String filtro)
	{
		String jpql = "SELECT u FROM Usuario u WHERE correo LIKE ?1 ";
		Query q = em.createQuery(jpql, Usuario.class);
		q.setParameter(1, "%"+filtro+"%");
		List<Usuario> usuarios = q.getResultList();
		return usuarios;
		
	}
	
	
	public List<Usuario> getusuarioCodigo(String correo,String password)
	{
		String jpql = "SELECT u FROM Usuario u WHERE correo LIKE ?1 AND password Like ?2 ";
		Query q = em.createQuery(jpql, Usuario.class);
		q.setParameter(1, "%"+correo);
		q.setParameter(2, "%"+password);
		List<Usuario> usuarios = q.getResultList();
		return usuarios;
		
	}

	

}
