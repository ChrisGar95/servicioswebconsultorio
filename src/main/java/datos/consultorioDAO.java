package datos;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import javax.persistence.Query;

import modelo.Consultorio;



@Stateless
public class consultorioDAO {
	
	@Inject
	private EntityManager em;
	
	public void insert(Consultorio consultorio)
	{
		em.persist(consultorio);
		
	}
	
	public void update(Consultorio consultorio)
	{
		em.merge(consultorio);
		
	}
	
	public void remove(int codigo)
	{
		Consultorio consultorio=this.read(codigo);
		em.remove(consultorio);
		
	}
	
	public Consultorio read(int codigo)
	{
		Consultorio c=em.find(Consultorio.class, codigo);
		return c;
	}
	
	
	//consulta a una BD, se hace refrencia a las entidades del paquete modelo, no a la BD
	public List<Consultorio> getConsultorios()
	{
		String jpql = "SELECT u FROM consultorio u";
		Query q = em.createQuery(jpql, Consultorio.class);
		List<Consultorio> consultorios = q.getResultList();
		return consultorios;
		
	}
	
	
	public List<Consultorio> getconsultoriosNombre(String filtro)
	{
		String jpql = "SELECT u FROM consultorio u WHERE nombre LIKE ?1 ";
		Query q = em.createQuery(jpql, Consultorio.class);
		q.setParameter(1, "%"+filtro+"%");
		List<Consultorio> consultorios = q.getResultList();
		return consultorios;
		
	}
	
	public List<Consultorio> getconsultoriosCorreo(String filtro)
	{
		String jpql = "SELECT u FROM consultorio u WHERE correo LIKE ?1 ";
		Query q = em.createQuery(jpql, Consultorio.class);
		q.setParameter(1, "%"+filtro+"%");
		List<Consultorio> consultorios = q.getResultList();
		return consultorios;
		
	}
	

}
