package services;

import java.util.HashMap;


import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import negocio.GestionDato;
import modelo.Consultorio;
import modelo.Usuario;

@Path("/usuarios")
public class UsuarioServiceREST {

	@Inject
	private GestionDato gd;
	
	@GET
	@Path("/listado")
	@Produces("application/json")
	public List<Usuario> getUsuarios(){
		return gd.getUsuarios();
	}
	
	@GET
	@Path("/listadoConsultorio")
	@Produces("application/json")
	public List<Consultorio> getConsultorios(){
		return gd.getConsultorios();
	}
	
	@POST
	@Path("/insertar")
	@Produces("application/json")
	@Consumes("application/json")
	public Response insertar(Usuario usuario) {

		Response.ResponseBuilder builder =  null;
		Map<String, String> data = new HashMap<>();
		
		try {
			gd.guardarUsuario(usuario);
			data.put("code", "1");
			data.put("message", "OK");
			builder = Response.status(Response.Status.OK).entity(data);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			data.put("code", "99");
			data.put("message", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(data);
		}
		
		return builder.build();		
	}
	
	@POST
	@Path("/insertarConsultorio")
	@Produces("application/json")
	@Consumes("application/json")
	public Response insertar(Consultorio consultorio) {

		Response.ResponseBuilder builder =  null;
		Map<String, String> data = new HashMap<>();
		
		try {
			gd.guardarConsultorio(consultorio);
			data.put("code", "1");
			data.put("message", "OK");
			builder = Response.status(Response.Status.OK).entity(data);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			data.put("code", "99");
			data.put("message", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(data);
		}
		
		return builder.build();		
	}
	
}
