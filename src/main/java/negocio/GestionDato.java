package negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import datos.consultorioDAO;
import datos.usuarioDAO;
import modelo.Consultorio;
import modelo.Usuario;

@Stateless
public class GestionDato 
{
	
	
	@Inject
	private usuarioDAO daoUsuario;
	private consultorioDAO daoConsultorio;
	
	

///////////////////////////////Gestion Usuarios///////////////////////////////

	public void guardarUsuario(Usuario usuario){
	
		if(daoUsuario.read(usuario.getCodigo())==null)
		{
			usuario.setCodigoConsultorio(1);
			daoUsuario.insert(usuario);
		}
		else
		{
			daoUsuario.update(usuario);
		}
		
	}

	public List<Usuario> getUsuarios(){
		return daoUsuario.getusuarios();
	}
	
	
	public List<Usuario> UsuarioCodigo(String correo,String password)
	{
		return daoUsuario.getusuarioCodigo(correo, password);
	}

	public List<Usuario> UsuarioCorreo(String correo)
	{
		return daoUsuario.getusuariosCorreo(correo);
	}
	
	public void eliminarUsuario(int codigo) throws Exception {
		Usuario aux = daoUsuario.read(codigo);
		if(aux==null)
			throw new Exception("Usuario no existe");
		else
			daoUsuario.remove(codigo);
	}
	
	
	
	
	public Usuario getUsuario(int codigo) throws Exception {
		Usuario aux = daoUsuario.read(codigo);
		if(aux==null)
			throw new Exception("Usuario no existe");
		else
			return aux;
	}
	
	public void actualizarUsuario(Usuario usuario) 
	{
		daoUsuario.update(usuario);
	}

	public usuarioDAO getDaoUsuario() {
		return daoUsuario;
	}

	public void setDaoUsuario(usuarioDAO daoUsuario) {
		this.daoUsuario = daoUsuario;
	}
	

///////////////////////////////Gestion Consultorios///////////////////////////////
	
	public void guardarConsultorio(Consultorio consultorio){
		daoConsultorio.insert(consultorio);
	}

	public List<Consultorio> getConsultorios(){
		return daoConsultorio.getConsultorios();
	}
	
	

	


}