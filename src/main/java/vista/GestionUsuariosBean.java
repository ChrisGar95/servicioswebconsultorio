package vista;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import modelo.Usuario;
import negocio.GestionDato;

@ManagedBean

@RequestScoped
public class GestionUsuariosBean{
	

	@Inject
	private GestionDato gd;
	
	private Usuario usuario = new Usuario();
	private List<Usuario> usuariosList;
	private int id;  		//Parametro para edicion
	private Usuario newUsuario;
	private boolean editing;
	private String password2;
	private String password1;
	private String correo;
	//private SesionConsultoriosBean sesionConsultorio;
	
	
	@PostConstruct
	public void init() {
		newUsuario = new Usuario();
		editing = false;
		usuariosList = gd.getUsuarios();
	}
	
	public void loadData() 
	{
		
		System.out.println("load data " + id);
		if(id==0)
			return;
		try {
			newUsuario = gd.getUsuario(id);
			editing = true;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			//FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
				//	e.getMessage(), "Error");
            //facesContext.addMessage(null, m);
		}
	}
	
	
	public String guardar()
	{
		String ventana="";
		
		try 
		{
			if(editing)
			{
				
				
				gd.actualizarUsuario(newUsuario);
				ventana="dashboard?faces-redirect=true";
			}
				
				
				
				
			
				
				
			else
			{
				usuariosList=gd.UsuarioCorreo(correo);
				
				if(usuariosList.size()>0)
				{
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Usuario ya existente"));
					ventana=null;
				}
				else
				{
					if(password1.equals(password2))
					{
						usuario.setCorreo(correo);
						usuario.setPassword(password1);
						gd.guardarUsuario(usuario);
						ventana="dashboard?faces-redirect=true";
					}
					else 
					{
						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Las contraseñas no coinciden"));
						ventana=null;
					}
				}
					
				
				
			}
				
				
				init();
			//return "dashBoard?faces-redirect=true";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error al Registrar"));
			e.printStackTrace();
		}		
		return ventana;
	}

	public String ListarUsuarios()
	{
		
		usuariosList=gd.getUsuarios();
		return "usuarios";
	}
	
/////////////////////////////////////////////////////////////////////////////////////
	
	public String eliminarUsuario(int codigo) {
		
		try {
			
			gd.eliminarUsuario(codigo);
			
			return "usuarios?faces-redirect=true";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();			
			
			
		}		
		return null;
	}
	
	public String editarUsuario(Usuario usuario) {
		editing = true;
		System.out.println(usuario);
		//newUsuario = usuario;
		return "EditarUsuario?faces-redirect=true&id="+usuario.getCodigo();
	}


	public GestionDato getGd() {
		return gd;
	}



	public void setGd(GestionDato gd) {
		this.gd = gd;
	}



	public Usuario getUsuario() {
		return usuario;
	}



	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getUsuariosList() {
		return usuariosList;
	}

	public void setUsuariosList(List<Usuario> usuariosList) {
		this.usuariosList = usuariosList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		System.out.println("id param " + id);
		this.id = id;
	}

	public Usuario getNewUsuario() {
		return newUsuario;
	}

	public void setNewUsuario(Usuario newUsuario) {
		this.newUsuario = newUsuario;
	}

	public boolean isEditing() {
		return editing;
	}

	public void setEditing(boolean editing) {
		this.editing = editing;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getPassword1() {
		return password1;
	}

	public void setPassword1(String password1) {
		this.password1 = password1;
	}

	
	

	
	
}