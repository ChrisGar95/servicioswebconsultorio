package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class ConexionMysql {
	
Connection con = null;
    
    
    public Connection getConexion()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            /*String url = "jdbc:oracle:thin:@localhost:1521:XE";
            String user = "root";
            String password = "QTSWrunv1681";*/
            
            String url = "jdbc:mysql://localhost:3306/consultoriomedico";
            con = DriverManager.getConnection(url, "root", "QTSWrunv1681");
        
            if(con!=null)
            {
            	System.out.println("CONEXION EXITOSA");
            }
            
            else
            {
            	System.out.println("ERROR CONEXION");
            }
            
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return con;
        
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

}
